const jwt = require('jsonwebtoken');
const {secret} = require("../config");

module.exports = function(req, res, next) {
  try {
    const token = req.headers.authorization.split(' ')[1];
    if(!token) {
      return res.status(400).json({message: 'User not authorised'});
    }
    const decoded = jwt.verify(token, secret);
    req.user = decoded;
    next();
  } catch (e) {
    res.status(400).json({message: 'User not authorised'});
  }
};