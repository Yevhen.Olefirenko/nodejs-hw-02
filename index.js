const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const authRouter = require('./routes/authRouter');
const userRouter = require('./routes/userRouter');
const notesRouter = require('./routes/notesRouter');

dotenv.config();

const app = express();
const PORT = process.env.PORT;

app.use(express.json());
app.use(morgan('combined'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

mongoose.connect('mongodb+srv://yevhen:yevhen@cluster0.v1wuv.mongodb.net/practice?retryWrites=true&w=majority',
    {
      useNewUrlParser: true,
    },
)

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: '));
db.once('open', function() {
  console.log('Connected successfully');
});

app.listen(PORT, () => console.log(`SERVER STARTED ON PORT ${PORT}`));
