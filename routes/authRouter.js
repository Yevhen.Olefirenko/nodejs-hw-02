const Router = require('express');
const router = new Router();
const {check} = require('express-validator');

const controller = require('../controllers/authController');

router.post('/register', [
    check('username', "Name is required").notEmpty(),
    check('password', "Password is required").notEmpty(),
], controller.registration);

router.post('/login', [
    check('username', "Name is required").notEmpty(),
    check('password', "Password is required").notEmpty(),
], controller.login);

module.exports = router;