const Router = require('express');
const router = new Router();
const authMiddleware = require('../middleware/authMiddleware');
const controller = require('../controllers/noteController');

router.get('/', authMiddleware, controller.getUsersNotes);
router.post('/', authMiddleware, controller.addNoteForUser);
router.get('/:id', authMiddleware, controller.getUsersNoteById);
router.put('/:id', authMiddleware, controller.updateUsersNote);
router.patch('/:id', authMiddleware, controller.checkUsersNote);
router.delete('/:id', authMiddleware, controller.deleteUsersNote);

module.exports = router;