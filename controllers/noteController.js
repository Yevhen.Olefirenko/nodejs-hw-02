const Note = require('../models/Note');

class noteController {
  async getUsersNotes(req, res) {
    try {
      let offset = 0;
      let limit = 10;

      if (req.query.offset) {
        offset = req.query.offset;
      }
      if (req.query.limit) {
        limit = req.query.limit;
      }
      const id = req.user.id;
      const notes = await Note.find({userid: id}, {__v: 0});

      notes.slice(offset, offset + limit);

      res.status(200).send({
        offset: offset,
        limit: limit,
        count: notes.length,
        notes: notes
      });
    } catch (e) {
      console.log(e)
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async addNoteForUser(req, res) {
    try {
      const {text} = req.body;
      const id = req.user.id;
      const note = new Note({
        userId: id,
        text: text,
      });

      await note.save();
      res.status(200).send({message: 'Success'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async getUsersNoteById(req, res) {
    try {
      const {id} = req.params;
      if (!id) {
        res.status(400).send({message: 'Bad request'});
      }
      const note = await Note.findById(id);
      if (!note) {
        res.status(400).send({message: 'Bad request'});
      }
      res.status(200).send({note: note});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async updateUsersNote(req, res) {
    try {
      const {id} = req.params;
      const {text} = req.body;

      if (!id) {
        res.status(400).send({message: 'Bad request'});
      }
      if (!text) {
        res.status(400).send({message: 'Bad request'});
      }
      const note = await Note.findById(id);
      if (!note) {
        res.status(400).send({message: 'Bad request'});
      }
      await Note.updateOne({_id: id}, {text: text});

      res.status(200).send({message: 'Success'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async checkUsersNote(req, res) {
    try {
      const {id} = req.params;

      if (!id) {
        res.status(400).send({message: 'Bad request'});
      }
      const note = await Note.findById(id);
      if (!note) {
        res.status(400).send({message: 'Bad request'});
      }
      await Note.updateOne({_id: id}, {completed: !note.completed});

      res.status(200).send({message: 'Success'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async deleteUsersNote(req, res) {
    try {
      const {id} = req.params;

      if (!id) {
        res.status(400).send({message: 'Bad request'});
      }
      const note = await Note.findById(id);
      if (!note) {
        res.status(400).send({message: 'Bad request'});
      }
      await Note.deleteOne({_id: id});

      res.status(200).send({message: 'Success'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }
}

module.exports = new noteController();

