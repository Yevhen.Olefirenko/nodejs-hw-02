const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');
const {secret} = require("../config");

const generateAccessToken = (id) => {
    const payload = {id};

    return jwt.sign(payload, secret, {expiresIn: "24h"})
}

class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({message: 'Bad request'});
            }
            const {username, password} = req.body;
            const hashPassword = bcrypt.hashSync(password, 7);
            const user = new User({username, password: hashPassword});
            await user.save();
            res.status(200).send({message: 'Success'});
        } catch (e) {
            res.status(500).json({message: 'Bad request'})
        }
    }

    async login(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({message: 'Bad request'});
            }
            const {username, password} = req.body;
            const user = await User.findOne({username});
            if (!user) {
                return res.status(400).json({message: 'Bad request'});
            }
            const validPassword = bcrypt.compareSync(password, user.password);
            if (!validPassword) {
                return res.status(400).json({message: 'Bad request'});
            }
            const token = generateAccessToken(user._id);
            return res.status(200).send({message: 'Success', jwt_token: token});
        } catch (e) {
            res.status(500).json({message: 'Bad request'})
        }
    }
}

module.exports = new authController();